/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package complexos;

import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author amartins
 */
public class ComplexosTest {
    
    public ComplexosTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    @Test
    public void testAdd3() {
        System.out.println("add, mas 3 numeros");
        Complexos z1 = new Complexos(1,1);
        Complexos z2 = new Complexos(1,1);
        Complexos z3 = new Complexos(1,1);
        Complexos expResult = new Complexos(3,3);
        Complexos result = z3.add(z1.add(z2));
        assertEquals(expResult, result);
    }
    
    /**
     * Test of multiply method, of class Complexos.
     */
    @Test
    public void testMultiply() {
        System.out.println("multiply");
        Complexos z = new Complexos(1,-1);
        Complexos instance = new Complexos(1,1);
        Complexos expResult = new Complexos(2,0);
        Complexos result = instance.multiply(z);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Complexos.
     */
    @Test
    public void testToString() {
        double a = 1, b = 2;
        System.out.println("toString");
        Complexos instance = new Complexos(a,b);
        String expResult = a + " + i" +b;
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of add method, of class Complexos.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        Complexos z = new Complexos(1,0);
        Complexos instance = new Complexos(1,1);
        Complexos expResult = new Complexos(2,1);
        Complexos result = instance.add(z);
        assertEquals(expResult, result);
    }

    /**
     * Test of getModulo method, of class Complexos.
     */
    @Test
    public void testGetModulo() {
        System.out.println("getModulo");
        Complexos instance = new Complexos(3,4);
        double expResult = 5;
        double result = instance.getModulo();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of compareTo method, of class Complexos.
     */
    @Test
    public void testCompareTo() {
        System.out.println("compareTo");
        Complexos c = new Complexos(3,4);
        Complexos instance = new Complexos(4,3);
        int expResult = 0;
        int result = instance.compareTo(c);
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class Complexos.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Complexos instance = new Complexos();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of equals method, of class Complexos.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object other = null;
        Complexos instance = new Complexos();
        boolean expResult = false;
        boolean result = instance.equals(other);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
