/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package complexos;

import java.util.ArrayList;
import javax.swing.JOptionPane;  
/**
 *
 * @author amartins
 */
public class ComplexosMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Complexos z1, z2, z3;
        double a,b;
        int NUM_ELEM = 30;
        
        Complexos vec[] = new Complexos[NUM_ELEM];
        
        z1 = new Complexos(1, 0);
        z2 = new Complexos(0, 1);
        
        z3 = z1.add(z2);
        System.out.println(z1.toString() + " + " + z2.toString()  + " = " + z3.toString());
        
        z3 = z1.multiply(z2);
        System.out.println(z1.toString() + " * " + z2.toString()  + " = " + z3.toString());
        
        a=10;
        b=0;
        for(int i =0; i<NUM_ELEM; i++) {
            vec[i] = new Complexos(a,b);
            a -= 0.5;
            b += 0.5;
        }
        
        System.out.println("Vetor não ordenado");
        for(Complexos c : vec) 
            System.out.println(c.toString());
        
        MySort.sort(vec);
        
        System.out.println("Vetor ordenado");
        for(Complexos c : vec) 
            System.out.println(c.toString());
    }
}
